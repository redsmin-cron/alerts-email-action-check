#!/usr/bin/env expect

log_user 0                   ;# turn off the usual output

#If it all goes pear shaped the script will timeout after 20 seconds.
set timeout 20
set username [lindex $argv 0]
set password [lindex $argv 1]

spawn openssl s_client -connect pop.gmail.com:995 -crlf -quiet

expect "+OK"
send "user $username\n"
expect "+OK"
send "pass $password\n"
expect "+OK *"
send "STAT\n"
expect "+OK *"

set BUFFER_OUTPUT $expect_out(buffer)     ;# print the results of the command
set EMAIL_COUNT [string range $BUFFER_OUTPUT [expr {[string first " " $BUFFER_OUTPUT] + 1}] [expr {[string last " " $BUFFER_OUTPUT]}]]

puts "$EMAIL_COUNT email(s)"
if { $EMAIL_COUNT == 0} {
  puts "Assertion Failed. The pop box should contain one email"
  exit 1
}

;# remove old emails
;# we should only have received ONE alert, but remove other emails if they were too many

send "RETR 1\n"
send "DELE 1\n"
send "RETR 2\n"
send "DELE 2\n"
send "RETR 3\n"
send "DELE 3\n"
send "RETR 4\n"
send "DELE 4\n"
send "RETR 5\n"
send "DELE 5\n"
send "RETR 6\n"
send "DELE 6\n"
send "RETR 7\n"
send "DELE 7\n"

send "QUIT\n"
expect "+OK"
